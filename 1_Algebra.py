import numpy as np
import theano.tensor as T
from theano import function
import theano
# a ** 2 + b ** 2 + 2 * a * b.

def simple():
    a = T.lscalar('a')
    b = T.lscalar('b')

    aa = T.power(a, 2)
    bb = T.power(b, 2)

    ab2 = a * b * 2

    sum = aa + bb + ab2

    f = function([a, b], sum)

    # a = 5, b = 15

    result = f(5, 15)
    assert result == 400
    print (result)

def compute_multiple_thing():
    a = T.lscalar('a')
    b = T.lscalar('b')

    plus = a + b
    minus = a - b

    f = function([a, b], [plus, minus])

    p, m = f(6, 4)
    assert p == 10
    assert m == 2
    print (p,m)

def share_variable():
    W = theano.shared(0, name='W')
    a = T.lscalar('a')
    sum = W + a
    f = theano.function([a], W, updates=[])

if __name__=="__main__":
    # simple()
    # compute_multiple_thing()
