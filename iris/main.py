from simple_seq_model import SeqModel
from sklearn import datasets
from sklearn.model_selection import train_test_split
import numpy as np

def to_one_hot(y):
    num_labels = len(np.unique(y))
    onehotY = np.eye(num_labels)[y]  # One liner trick!
    return onehotY

if __name__ == "__main__":
    iris = datasets.load_iris()
    X = iris.data
    y = iris.target


    X_train, X_test, y_train, y_test = train_test_split(
                X, y, test_size = 0.33, random_state = 42)

    y_train = to_one_hot(y_train)
    y_test_onehot = to_one_hot(y_test)




    model = SeqModel(4, 3)
    #result = model.forward(X_test)
    #prediction = model.predict(X_test)
    #loss = model.cal_cost(X_test, y_test)
    #print result
    #print('prediction')
    #print(prediction)
    #print('loss')
    #print(loss)
    n = len(X_train)
    MAX_EPOCH = 10000
    LEARNING_RATE = 0.05


    for e in range(0, MAX_EPOCH):
        print ('epoch %d' %(e))
        model.sgd_step(X_train, y_train, LEARNING_RATE)
        cost = model.cal_cost(X_train, y_train)
        print ('cost %f'%(cost))

    test_cost = model.cal_cost(X_test, y_test_onehot)
    print ('test cost %f'%(cost))

    # acc
    prediction = model.predict(X_test)
    prediction = np.array(prediction)
    y_test = np.array(y_test)
    correct = sum(prediction==y_test)
    acc = float(correct)/len(prediction)
    print('correct %f'%(acc))

    print ('meow')