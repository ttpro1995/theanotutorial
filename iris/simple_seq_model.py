import theano
import theano.tensor as T
import numpy as np

class SeqModel:
    def __init__(self, size_in, size_out):
        self.size_in = size_in
        self.size_out = size_out
        self.hidden_size = 256

        W = np.random.uniform(-np.sqrt(1. / size_in), np.sqrt(1. / size_in), (size_in, self.hidden_size))  # 4x256
        U = np.random.uniform(-np.sqrt(1./size_in), np.sqrt(1./size_in), (self.hidden_size, size_out)) # 256x3

        b = np.zeros(self.hidden_size)
        c = np.zeros(size_out)


        self.W = theano.shared(W.astype(theano.config.floatX), name='W')
        self.U = theano.shared(U.astype(theano.config.floatX), name='U')

        self.b = theano.shared(b.astype(theano.config.floatX), name='b')
        self.c = theano.shared(c.astype(theano.config.floatX), name='c')

        self.__theano_build__()

    def __theano_build__(self):
        W = self.W
        U = self.U
        b = self.b
        c = self.c

        x = T.fmatrix('x')
        y = T.fmatrix('y')
        learning_rate = T.scalar('learning_rate')

        def forwardprop():
            h = T.nnet.sigmoid(T.dot(x,W) + b) # (Nx4)(4x256) =>(Nx256)
            o = T.nnet.softmax(T.dot(h,U) + c) # (Nx256)(256x3) => (Nx3)
            return o

        o = forwardprop()
        prediction = T.argmax(o, axis=1)
        cost = T.mean(T.nnet.categorical_crossentropy(o, y))


        # grad
        dW = T.grad(cost, W)
        dU = T.grad(cost, U)
        dc = T.grad(cost, c)
        db = T.grad(cost, b)



        self.forward = theano.function([x], o, allow_input_downcast=True)
        self.predict = theano.function([x], prediction, allow_input_downcast=True)
        self.cal_cost = theano.function([x, y], cost, allow_input_downcast=True)
        self.sgd_step = theano.function([x, y, learning_rate], [],
                                        updates=[(self.W, self.W - learning_rate*dW)
                                                 ,(self.U, self.U - learning_rate*dU)
                                                 ,(self.b, self.b - learning_rate*db)
                                                 ,(self.c, self.c - learning_rate*dc)],
                                        allow_input_downcast=True
                                        )
